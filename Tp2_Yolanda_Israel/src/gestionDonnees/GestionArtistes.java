package gestionDonnees;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class GestionArtistes {

	private Connection connection = ControleConnexion.getLaConnexion();
	private ArrayList<Artistes> listeArtistes;
	private ArrayList<Artistes> listeRecherche;
	private ArrayList<Album> listeAlbum;

	public GestionArtistes() {
		listeArtistes = obtenirListeArtistes();

	}

	public ArrayList<Artistes> getListeArtistes() {
		return listeArtistes;
	}

	private ArrayList<Artistes> obtenirListeArtistes() {
		ArrayList<Artistes> liste = new ArrayList<Artistes>();
		Statement statement=null;
		ResultSet jeuResultat=null;
		String requete = "SELECT * FROM Artiste ORDER BY nom";

		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			while (jeuResultat.next()) {
				String numero = Integer.toString(jeuResultat.getInt("id"));
				String nom = jeuResultat.getString("nom");
				boolean membre = jeuResultat.getBoolean("membre");
				String nomPhoto = jeuResultat.getString("photo");
				liste.add(new Artistes(numero,nom, membre, nomPhoto));

			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl�me rencontr\u00E8 : "
					+ sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}

		return liste;
	}

	public boolean ajouterArtistesBD (Artistes artiste){
		int intMembre=0;
		boolean boolAjout = false;
		
		if(artiste.isMembre()){
			intMembre=1;
		}
		String requete = "INSERT INTO Artiste(nom,membre,photo) VALUES ('" 
				+artiste.getNom() + "','"
				+intMembre+ "','"
				+ artiste.getNomPhoto() +"')";

		try{
			Statement statement = connection.createStatement();
			statement.executeUpdate(requete);
			boolAjout=true;
		}catch(SQLException sqle){
			JOptionPane.showMessageDialog(null, "Probl\u00E8me rencontr\u00E9 lors de l'enregistrement de l'artiste: " + sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}
		return boolAjout;
	}
	
	public boolean supprimerArtisteBD(Artistes artiste){
		boolean boolSupprime=false;
		String requete = "DELETE FROM Artiste where id="+artiste.getNumero();
		
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(requete);
			boolSupprime=true;
			
		}catch(SQLException sqle){
			JOptionPane.showMessageDialog(null, "Probl\u00E8me rencontr\u00E9 lors de la suppression de l'artiste: " + sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
	}
		return boolSupprime;

	}
	
	public boolean modifierArtisteBD(Artistes artiste){
		int intMembre=0;
		boolean boolModif = false;
		
		if(artiste.isMembre()){
			intMembre=1;
		}
		String requete = "UPDATE Artiste SET nom='"+artiste.getNom()+"',membre='"+intMembre+"' WHERE id='"+artiste.getNumero()+"'";
		
		try{
			Statement statement = connection.createStatement();
			statement.executeUpdate(requete);
			boolModif = true;
		}catch(SQLException sqle){
			JOptionPane.showMessageDialog(null, "Probl\u00E8me rencontr\u00E9 lors de la modification de l'artiste: " + sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}
		return boolModif;
	}
	
	public String getId(Artistes artiste){
		String id = "";
		Statement statement=null;
		ResultSet jeuResultat=null;
		String requete = "SELECT id FROM Artiste WHERE nom='"+artiste.getNom()+"'";

		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			jeuResultat.next();
				id = Integer.toString(jeuResultat.getInt("id"));
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl�me rencontr\u00E9 : "
					+ sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}

		return id;
	}

	public ArrayList<Artistes> rechercherArtiste(String id, String name) {
ArrayList<Artistes> liste = new ArrayList<Artistes>();
		String requete;
		Statement statement=null;
		ResultSet jeuResultat=null;
		
		if(!id.isEmpty()){
				int num = Integer.parseInt(id);
				requete = "SELECT * FROM Artiste WHERE id LIKE '%"+num+"%' AND nom LIKE '%"+name+"%'";
		} else {
		requete = "SELECT * FROM Artiste WHERE id LIKE '%"+id+"%' AND nom LIKE '%"+name+"%'";
		}

		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			while (jeuResultat.next()) {
				String numero = Integer.toString(jeuResultat.getInt("id"));
				String nom = jeuResultat.getString("nom");
				boolean membre = jeuResultat.getBoolean("membre");
				String nomPhoto = jeuResultat.getString("photo");
				liste.add(new Artistes(numero,nom, membre, nomPhoto));

			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl�me rencontr\u00E8 : "
					+ sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}

		return liste;
	}
}

package gestionDonnees;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class GestionUtilisateur {

	private Connection connection = new ControleConnexion().getLaConnexion();
	private ArrayList<Utilisateur> listeUtilisateurs;
	
	public GestionUtilisateur (){
		listeUtilisateurs = obtenirListeDesUtilisateurs();
	}
	
	public ArrayList<Utilisateur> getListeUtilisateur(){
		return listeUtilisateurs;
	}
	
	public ArrayList<Utilisateur> obtenirListeDesUtilisateurs(){
		ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
		Statement statement=null;
		ResultSet jeuResultat=null;
		String requete = "SELECT * FROM Utilisateurs";
		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			while (jeuResultat.next()) {
				String nomUtilisateur = jeuResultat.getString("utilisateur");
				String motDepasse = jeuResultat.getString("password");
				liste.add(new Utilisateur(nomUtilisateur,motDepasse));

			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl�me rencontr\u00E8 : "
					+ sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}

		return liste;
		
	}
	
}

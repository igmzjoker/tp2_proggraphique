package gestionDonnees;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;


public class ModeleArtiste extends AbstractTableModel{
	private ArrayList<Artistes> lesDonnees ;
	private final String[] lesTitres = {"Num�ro", "Nom", "Membre"}; 
	
	public ModeleArtiste(ArrayList<Artistes> lesDonnees) {
		this.lesDonnees=lesDonnees;	
	}
	
	public void ajouterArtiste(Artistes artiste) {
		lesDonnees.add(artiste);
		fireTableRowsInserted(lesDonnees.size()-1, lesDonnees.size()-1);
		
	}
	
	public void modifierArtiste(int index,Artistes artiste){
		lesDonnees.set(index, artiste);
		fireTableRowsUpdated(index, index);
	}
	
	public void supprimerArtiste(Artistes artiste) {
		lesDonnees.remove(artiste);
		fireTableRowsDeleted(lesDonnees.size()-1, lesDonnees.size() - 1);
		
	}
	

	@Override
	public int getColumnCount() {
		return lesTitres.length;
	}

	@Override
	public int getRowCount() {
		return lesDonnees.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return lesDonnees.get(rowIndex).getNumero();
		case 1 :
			return lesDonnees.get(rowIndex).getNom();
		case 2 :
			return lesDonnees.get(rowIndex).isMembre();
		default:
			return null;
		}
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return lesTitres[columnIndex];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
	
		  switch (columnIndex) {
	        case 0: return String.class;
	        case 1: return String.class;
	        case 2: return Boolean.class;
	        default: throw new IllegalArgumentException(" index de colonne invalide: "+columnIndex);
		  }
	}
	
	public void  setLesDonnees(ArrayList<Artistes> lesDonnees){
		this.lesDonnees = lesDonnees;
		fireTableDataChanged();
	}

}

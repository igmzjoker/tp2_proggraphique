package gestionDonnees;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class ControleConnexion {
	private static Connection laConnexion;
	private static String url = "jdbc:mysql://localhost/Yolanda_Israel_album";
	private static String usager ="root";
	private static String mdp = "mysql";

	public static void connecter() {
		try {
			if (laConnexion == null || laConnexion.isClosed()) {
				Class.forName("org.gjt.mm.mysql.Driver");
				laConnexion = DriverManager.getConnection(url,usager,mdp);
				}
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Erreur!! Pilote manquant");
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Erreur SQL" + sqle);
		}
	}

	public static void fermerSession() {
		try {
			if(laConnexion!= null && !laConnexion.isClosed()){
				laConnexion.isClosed();
				
			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Erreur SQL" + sqle);
		}
	}

	public static Connection getLaConnexion() {
		return laConnexion;

	}
}


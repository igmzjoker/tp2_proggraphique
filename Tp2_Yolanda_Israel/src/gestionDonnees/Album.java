package gestionDonnees;

public class Album {
	private int id;
	private String titre;
	private double prix;
	private String genre;
	private String anneeDeSortie;
	private String maisonDistribution;
	private String image;
	private int numeroArtiste;
	
	public Album(int id,String titre, double prix, String genre, String anneeDeSortie, String maisonDistribution, String image, int numeroArtise){
		setId(id);
		setTitre(titre);
		setPrix(prix);
		setGenre(genre);
		setAnneeDeSortie(anneeDeSortie);
		setMaisonDistribution(maisonDistribution);
		setImage(image);
		setNumeroArtiste(numeroArtise);
	}
	
	public int getId() {
		return this.id ;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAnneeDeSortie() {
		return anneeDeSortie;
	}

	public void setAnneeDeSortie(String anneeDeSortie) {
		this.anneeDeSortie = anneeDeSortie;
	}

	public String getMaisonDistribution() {
		return maisonDistribution;
	}

	public void setMaisonDistribution(String maisonDistribution) {
		this.maisonDistribution = maisonDistribution;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getNumeroArtiste() {
		return numeroArtiste;
	}

	public void setNumeroArtiste(int numeroArtiste) {
		this.numeroArtiste = numeroArtiste;
	}
	
	@Override
	public String toString() {
		return "Titre : " + getTitre() + ". Ann�e: " + getAnneeDeSortie();
	}
	
	
}

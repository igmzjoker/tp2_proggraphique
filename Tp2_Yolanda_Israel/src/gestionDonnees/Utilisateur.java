package gestionDonnees;

public class Utilisateur {
	private String utilisateur;
	private String password;
	
	public Utilisateur(String utilisateur, String password){
		setUtilisateur(utilisateur);
		setPassword(password);
	}

	public String getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}

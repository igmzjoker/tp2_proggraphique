package gestionDonnees;

public class Artistes {

	private String numero;
	private String nom;
	private boolean membre;
	private String nomPhoto;
	
	
	
	public Artistes(String numero,String nom, boolean membre, String nomPhoto) {
		setNumero(numero);
		setNom(nom);
		setMembre(membre);
		setNomPhoto(nomPhoto);
	}
	
	public Artistes(String nom, boolean membre, String nomPhoto){
		setNom(nom);
		setMembre(membre);
		setNomPhoto(nomPhoto);
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero){
		this.numero = numero;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public boolean isMembre() {
		return membre;
	}
	public void setMembre(boolean membre) {
		this.membre = membre;
	}
	public String getNomPhoto() {
		return nomPhoto;
	}
	
	public void setNomPhoto(String nomPhoto){
		this.nomPhoto = nomPhoto;
	}

}

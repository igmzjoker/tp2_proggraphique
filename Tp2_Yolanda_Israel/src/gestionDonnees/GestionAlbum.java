package gestionDonnees;
import java.sql.*;
import java.util.*;
import javax.swing.*;

public class GestionAlbum {
	private Connection connection = ControleConnexion.getLaConnexion();
	private ArrayList<Album> listeAlbum;

	public GestionAlbum() {
		listeAlbum = obtenirListeAlbum();
	}

	public ArrayList<Album> getListeAlbum() {
		return listeAlbum;
	}

	private ArrayList<Album> obtenirListeAlbum() {

		ArrayList<Album> liste = new ArrayList<Album>();
		Statement statement = null;
		ResultSet resultSet = null;
		String requete = "SELECT * FROM Album ORDER BY id ";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(requete);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String titre = resultSet.getString("titre");
				double prix = resultSet.getDouble("prix");
				String genre = resultSet.getString("genre");
				String anneePlubication = resultSet.getString("anne");
				String maisonDistrib = resultSet.getString("maisondist");
				String image = resultSet.getString("image");
				int numeroArtiste = resultSet.getInt("id_artiste");
				liste.add(new Album(id, titre, prix, genre, anneePlubication, maisonDistrib, image, numeroArtiste));
			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probleme rencontre : " + sqle.getMessage(), "Resultat",
					JOptionPane.ERROR_MESSAGE);
		}

		return liste;
	}
	
	public ArrayList<Album> obtenirListeAlbumArtiste(int idArtiste) {

		ArrayList<Album> listeAlbum = new ArrayList<Album>();
		Statement statement = null;
		ResultSet resultSet = null;
		String requete = "SELECT * FROM Album WHERE id_artiste="+idArtiste;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(requete);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String titre = resultSet.getString("titre");
				double prix = resultSet.getDouble("prix");
				String genre = resultSet.getString("genre");
				String anneePlubication = resultSet.getString("anne");
				String maisonDistrib = resultSet.getString("maisondist");
				String image = resultSet.getString("image");
				int numeroArtiste = resultSet.getInt("id_artiste");
				listeAlbum.add(new Album(id, titre, prix, genre, anneePlubication, maisonDistrib, image, numeroArtiste));
			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probleme rencontre : " + sqle.getMessage(), "Resultat",
					JOptionPane.ERROR_MESSAGE);
		}

		return listeAlbum;
	}
	
	public boolean ajouterAlbum(Album album){
		
		boolean boolAjout= false;
		
		String requete ="INSERT INTO Album(titre, prix, genre, anne, maisondist, image, id_artiste) VALUES ('" + album.getTitre() + "','"		
		+ album.getPrix() + "','" 
		+ album.getGenre() +"','"
		+ album.getAnneeDeSortie() + "','" 
		+ album.getMaisonDistribution() + "','"
		+ album.getImage() + "','" 
		+ album.getNumeroArtiste() +"')";
		
		try{
			
			Statement statement = connection.createStatement();
			statement.executeUpdate(requete);
			boolAjout = true;
			
		} catch(SQLException sqle) {
			JOptionPane.showMessageDialog(null, "probleme recontre lors de l'enregistrement de l'album : " + sqle.getMessage(),"Resultat",JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return boolAjout;
	
	}
	
	public boolean supprimerAlbum(Album album){
		
		boolean boolSupprimer = false;
		int id = album.getId();
		
		String requete = "DELETE FROM Album WHERE id = "+album.getId();
		
		try{
			Statement statement = connection.createStatement();
			statement.executeUpdate(requete);
			boolSupprimer = true;
		} catch(SQLException sqle){
			JOptionPane.showMessageDialog(null, "probleme recontre lors de la suppression de l'album : " + sqle.getMessage(),"Resultat",JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return boolSupprimer;
	}
	
	public boolean modifierAlbum(Album album){
		boolean boolModifier = false;
		
		String requete = "UPDATE Album SET titre = ?, prix = ?, genre = ?, anne = ?,maisondist = ?, image = ?,id_artiste = ? WHERE id = ?";
		
		try{
			PreparedStatement preparedStatement = connection.prepareStatement(requete);
			
			preparedStatement.setString(1, album.getTitre());
			preparedStatement.setDouble(2, album.getPrix());
			preparedStatement.setString(3, album.getGenre());
			preparedStatement.setString(4, album.getAnneeDeSortie());
			preparedStatement.setString(5, album.getMaisonDistribution());
			preparedStatement.setString(6, album.getImage());
			preparedStatement.setInt(7, album.getNumeroArtiste());
			preparedStatement.setInt(8, album.getId());
			
			preparedStatement.executeUpdate();
			
			boolModifier = true;
			
		}catch(SQLException sqle){
			
			JOptionPane.showMessageDialog(null, "probleme recontre lors de la modification de l'album : " + sqle.getMessage(),"Resultat",JOptionPane.ERROR_MESSAGE);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return boolModifier;
	}
	public ArrayList<Album> rechercherAlbum(String id){
		ArrayList<Album> listeAlbum = new ArrayList<Album>();
		Statement statement = null;
		ResultSet resultSet = null;
		String requete = "SELECT * FROM Album WHERE id LIKE '%"+id+"%'";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(requete);
			while (resultSet.next()) {
				int id1 = resultSet.getInt("id");
				String titre = resultSet.getString("titre");
				double prix = resultSet.getDouble("prix");
				String genre = resultSet.getString("genre");
				String anneePlubication = resultSet.getString("anne");
				String maisonDistrib = resultSet.getString("maisondist");
				String image = resultSet.getString("image");
				int numeroArtiste = resultSet.getInt("id_artiste");
				listeAlbum.add(new Album(id1, titre, prix, genre, anneePlubication, maisonDistrib, image, numeroArtiste));
			}
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl\u00E8me rencontr\u00E9 : " + sqle.getMessage(), "R\u00E9sultat",
					JOptionPane.ERROR_MESSAGE);
		}

		return listeAlbum;
	}
	
	
	public int getId(Album album){
		int id=0;
		Statement statement=null;
		ResultSet jeuResultat=null;
		String requete = "SELECT id FROM Album WHERE titre='"+album.getTitre()+"'";

		try {
			statement = connection.createStatement();
			jeuResultat = statement.executeQuery(requete);
			jeuResultat.next();
				id = jeuResultat.getInt("id");
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Probl\u00E8me rencontr\u00E9 : "
					+ sqle.getMessage(), "R\u00E9sultat", JOptionPane.ERROR_MESSAGE);
		}

		return id;
	}



}

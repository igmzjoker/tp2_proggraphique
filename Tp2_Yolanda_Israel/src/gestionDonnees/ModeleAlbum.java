package gestionDonnees;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;


public class ModeleAlbum extends AbstractTableModel{
	private ArrayList<Album> lesDonnees ;
	private final String[] lesTitres = {"Numero", "Titre", "Prix","Genre","Annee de sortie","Maison de distribution","Image"," Numero Artiste"}; 	
	
	public ModeleAlbum(ArrayList<Album> lesDonnees) {
		this.lesDonnees=lesDonnees;	
	}
	
	public void ajouterAlbum(Album album){
		lesDonnees.add(album);
		fireTableRowsInserted(lesDonnees.size()-1, lesDonnees.size()-1);
	}
	
	public void modifierAlbum(int index, Album album){
		lesDonnees.set(index, album);
		fireTableRowsUpdated(index, index);
	}
	
	public void supprimerAlbumBD(Album album){
		lesDonnees.remove(album);
		fireTableRowsDeleted(lesDonnees.size()-1, lesDonnees.size() - 1);
	}
	@Override
	public int getColumnCount() {
		return lesTitres.length;
	}

	@Override
	public int getRowCount() {
		return lesDonnees.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return lesDonnees.get(rowIndex).getId();
		case 1 :
			return lesDonnees.get(rowIndex).getTitre();
		case 2 :
			return lesDonnees.get(rowIndex).getPrix();
		case 3:
			return lesDonnees.get(rowIndex).getGenre();
		case 4 :
			return lesDonnees.get(rowIndex).getAnneeDeSortie();
		case 5 :
			return lesDonnees.get(rowIndex).getMaisonDistribution();
		case 6 :
			return lesDonnees.get(rowIndex).getImage();
		case 7 :
			return lesDonnees.get(rowIndex).getNumeroArtiste();	
		default:
			return null;
		}
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return lesTitres[columnIndex];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
	
		  switch (columnIndex) {
	        case 0: return Integer.class;
	        case 1: return String.class;
	        case 2: return Double.class;
	        case 3: return String.class;
	        case 4: return String.class;
	        case 5: return String.class;
	        case 6: return String.class;
	        case 7: return Integer.class;
	        default: throw new IllegalArgumentException(" index de colonne invalide: "+columnIndex);
		  }
	}

	public void setLesDonnees(ArrayList<Album> lesDonnees){
		this.lesDonnees = lesDonnees;
		fireTableDataChanged();
	}
}

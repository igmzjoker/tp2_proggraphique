import gestionDonnees.ControleConnexion;
import interfaceGraphique.FrmConnexion;

public class MainTP02 {

	public static void main(String[] args) {
		ControleConnexion.connecter();
		FrmConnexion frmConnexion = new FrmConnexion();
		frmConnexion.setVisible(true);
	}

}

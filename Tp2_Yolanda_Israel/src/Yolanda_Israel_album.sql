-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 25 Avril 2016 à 01:01
-- Version du serveur :  5.6.28
-- Version de PHP :  5.5.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Yolanda_Israel_album`
--

-- --------------------------------------------------------

--
-- Structure de la table `Album`
--

CREATE TABLE IF NOT EXISTS `Album` (
  `id` int(4) NOT NULL,
  `titre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prix` double(4,2) NOT NULL,
  `genre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `anne` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `maisondist` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `id_artiste` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Album`
--

INSERT INTO `Album` (`id`, `titre`, `prix`, `genre`, `anne`, `maisondist`, `image`, `id_artiste`) VALUES
(1, 'Purpose', 19.99, 'Dance pop', '2015', 'Def Jam Recordings', 'purpose.jpg', 4),
(2, '25', 14.99, 'soul', '2015', 'XL-Columbia', 'adeleAlbum.jpg', 2),
(3, 'Dark side of the moon', 17.99, 'rock progressif', '1973', 'Harvest', 'dsm.jpg', 5),
(4, 'Thriller', 20.00, 'Funk', '1982', 'Epic', 'thriller.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Artiste`
--

CREATE TABLE IF NOT EXISTS `Artiste` (
  `id` int(11) NOT NULL,
  `nom` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `membre` tinyint(1) NOT NULL,
  `photo` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Artiste`
--

INSERT INTO `Artiste` (`id`, `nom`, `membre`, `photo`) VALUES
(1, 'Michael Jackson', 1, 'michael.jpg'),
(2, 'Adele', 0, 'adele.jpg'),
(4, 'Justin Bieber', 1, 'justin.jpg'),
(5, 'Pink Floyd', 0, 'pinkFloyd.jpg'),
(10, 'rhianna', 1, 'rihanna.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateurs`
--

CREATE TABLE IF NOT EXISTS `Utilisateurs` (
  `utilisateur` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Utilisateurs`
--

INSERT INTO `Utilisateurs` (`utilisateur`, `password`) VALUES
('Yolanda', 'Dorvilier'),
('Israel', 'Gomez');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Album`
--
ALTER TABLE `Album`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Artiste`
--
ALTER TABLE `Artiste`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Album`
--
ALTER TABLE `Album`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `Artiste`
--
ALTER TABLE `Artiste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

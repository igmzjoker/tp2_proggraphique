package interfaceGraphique;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Apropos extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private FrmMenu parent;

	/**
	 * Create the dialog.
	 */
	public Apropos(FrmMenu parent) {
		super( parent, true );
		setTitle("\u00C0 Propos");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 228);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		this.parent=parent;
		setLocationRelativeTo(parent);
		{
			JLabel label = new JLabel("Nom de l'application : Gestion d'album");
			label.setBounds(189, 84, 243, 16);
			label.setHorizontalAlignment(SwingConstants.RIGHT);
			label.setFont(new Font("Tahoma", Font.BOLD, 13));
			contentPanel.add(label);
		}
		{
			JLabel label = new JLabel("Cr\u00E9ateurs : Israel Gomez et Yolanda Dorvilier-Damis");
			label.setBounds(95, 165, 337, 16);
			label.setHorizontalAlignment(SwingConstants.RIGHT);
			label.setFont(new Font("Tahoma", Font.BOLD, 13));
			contentPanel.add(label);
		}
		{
			JLabel label = new JLabel("Version : 1.0.3");
			label.setBounds(330, 111, 94, 16);
			label.setHorizontalAlignment(SwingConstants.RIGHT);
			label.setFont(new Font("Tahoma", Font.BOLD, 13));
			contentPanel.add(label);
		}
		{
			JLabel lblDernireModification = new JLabel("Derni\u00E8re modification : 2016-04-25");
			lblDernireModification.setBounds(156, 138, 268, 16);
			lblDernireModification.setHorizontalAlignment(SwingConstants.RIGHT);
			lblDernireModification.setFont(new Font("Tahoma", Font.BOLD, 13));
			contentPanel.add(lblDernireModification);
		}
		{
			JLabel label = new JLabel("");
			label.setIcon(new ImageIcon(Apropos.class.getResource("/images/album2.png")));
			label.setBounds(10, 11, 147, 130);
			contentPanel.add(label);
		}
		{
			JLabel lblPropos = new JLabel("\u00C0 PROPOS");
			lblPropos.setHorizontalAlignment(SwingConstants.CENTER);
			lblPropos.setFont(new Font("Tahoma", Font.BOLD, 19));
			lblPropos.setBounds(189, 11, 163, 31);
			contentPanel.add(lblPropos);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 228, 434, 33);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new OkButtonActionListener());
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

	private class OkButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}
}

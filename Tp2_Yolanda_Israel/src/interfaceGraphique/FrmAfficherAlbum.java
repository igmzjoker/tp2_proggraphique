package interfaceGraphique;

import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JButton;
import javax.swing.JTable;

import gestionDonnees.Album;
import gestionDonnees.Artistes;
import gestionDonnees.GestionAlbum;
import gestionDonnees.GestionArtistes;
import gestionDonnees.ModeleAlbum;

import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FrmAfficherAlbum extends JFrame {

	private JPanel contentPane;
	private JPanel pnlMenu;
	private JLabel lblAjouter;
	private JLabel lblModifier;
	private JLabel lblSupprimer;
	private JLabel lblRechercher;
	private JLabel lblQuitter;
	private JPanel pnlCenter;
	private JPanel pnlArtiste;
	private JPanel pnlinfo;
	private JLabel lblNumro;
	private JLabel lblNom;
	private JLabel lblMembre;
	private JPanel pnlPhoto;
	private JLabel lblPhoto;
	private ArrayList<Artistes> listArtiste;
	private JPanel pnlField;
	private JScrollPane scrollPane;
	private JTextField txtNumero;
	private JTextField textField;
	private JTextField textField_1;
	private static GestionAlbum liste;
	private static ModeleAlbum modeleAlbum;
	private JTable tableAlbum;
	private JLabel lblGenre;
	private JLabel lblAnneeDistribution;
	private JLabel lblMaisonDistribution;
	private JLabel lblNumroArtiste;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JComboBox comboBox;
	private JButton button;

	/**
	 * Create the frame.
	 */
	public FrmAfficherAlbum() {
		addWindowListener(new ThisWindowListener());
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmAfficherAlbum.class.getResource("/images/album2.png")));
		setTitle("Gestion des albums");
		setBounds(100, 100, 544, 364);
		setLocationRelativeTo(null);
		liste = new GestionAlbum();
		modeleAlbum = new ModeleAlbum(liste.getListeAlbum());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanel_2(), BorderLayout.WEST);
		contentPane.add(getPanel_1_1(), BorderLayout.CENTER);

	}

	private JPanel getPanel_2() {
		if (pnlMenu == null) {
			pnlMenu = new JPanel();
			pnlMenu.setLayout(new GridLayout(5, 1, 0, 0));
			pnlMenu.add(getLblAjouter());
			pnlMenu.add(getLblModifier());
			pnlMenu.add(getLblSupprimer());
			pnlMenu.add(getLblRechercher());
			pnlMenu.add(getLblQuitter());
		}
		return pnlMenu;
	}

	private JLabel getLblAjouter() {
		if (lblAjouter == null) {
			lblAjouter = new JLabel("Ajouter");
			lblAjouter.addMouseListener(new LblMouseListener());
			lblAjouter.setIcon(new ImageIcon(FrmAfficherAlbum.class.getResource("/images/add.png")));
			lblAjouter.setHorizontalAlignment(SwingConstants.CENTER);
			lblAjouter.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblAjouter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		return lblAjouter;
	}

	private JLabel getLblModifier() {
		if (lblModifier == null) {
			lblModifier = new JLabel("Modifier");
			lblModifier.addMouseListener(new LblMouseListener());
			lblModifier.setIcon(new ImageIcon(FrmAfficherAlbum.class.getResource("/images/edit.png")));
			lblModifier.setHorizontalAlignment(SwingConstants.CENTER);
			lblModifier.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblModifier.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		return lblModifier;
	}

	private JLabel getLblSupprimer() {
		if (lblSupprimer == null) {
			lblSupprimer = new JLabel("Supprimer");
			lblSupprimer.addMouseListener(new LblMouseListener());
			lblSupprimer.setIcon(new ImageIcon(FrmAfficherAlbum.class.getResource("/images/delete.png")));
			lblSupprimer.setHorizontalAlignment(SwingConstants.CENTER);
			lblSupprimer.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblSupprimer.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		return lblSupprimer;
	}

	private JLabel getLblRechercher() {
		if (lblRechercher == null) {
			lblRechercher = new JLabel("Rechercher");
			lblRechercher.addMouseListener(new LblMouseListener());
			lblRechercher.setIcon(new ImageIcon(FrmAfficherAlbum.class.getResource("/images/search.png")));
			lblRechercher.setHorizontalAlignment(SwingConstants.CENTER);
			lblRechercher.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRechercher.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		return lblRechercher;
	}

	private JLabel getLblQuitter() {
		if (lblQuitter == null) {
			lblQuitter = new JLabel("Quitter");
			lblQuitter.addMouseListener(new LblMouseListener());
			lblQuitter.setIcon(new ImageIcon(FrmAfficherAlbum.class.getResource("/images/icon_quit.gif")));
			lblQuitter.setHorizontalAlignment(SwingConstants.CENTER);
			lblQuitter.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblQuitter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		return lblQuitter;
	}

	private JPanel getPanel_1_1() {
		if (pnlCenter == null) {
			pnlCenter = new JPanel();
			pnlCenter.setLayout(new BorderLayout(0, 0));
			pnlCenter.add(getPanel_3(), BorderLayout.NORTH);
			pnlCenter.add(getScrollPane(), BorderLayout.CENTER);
		}
		return pnlCenter;
	}

	private JPanel getPanel_3() {
		if (pnlArtiste == null) {
			pnlArtiste = new JPanel();
			pnlArtiste.setLayout(new BorderLayout(10, 0));
			pnlArtiste.add(getPanel_1_2(), BorderLayout.WEST);
			pnlArtiste.add(getPanel_2_1(), BorderLayout.EAST);
			pnlArtiste.add(getPanel_4(), BorderLayout.CENTER);
		}
		return pnlArtiste;
	}

	private JPanel getPanel_1_2() {
		if (pnlinfo == null) {
			pnlinfo = new JPanel();
			pnlinfo.setLayout(new GridLayout(7, 1, 10, 0));
			pnlinfo.add(getLblNumro());
			pnlinfo.add(getLblNom());
			pnlinfo.add(getLblMembre());
			pnlinfo.add(getLblGenre());
			pnlinfo.add(getLblAnneeDistribution());
			pnlinfo.add(getLblMaisonDistribution());
			pnlinfo.add(getLblNumroArtiste());
		}
		return pnlinfo;
	}

	private JLabel getLblNumro() {
		if (lblNumro == null) {
			lblNumro = new JLabel("Num\u00E9ro");
			lblNumro.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNumro;
	}

	private JLabel getLblNom() {
		if (lblNom == null) {
			lblNom = new JLabel("Titre");
			lblNom.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNom;
	}

	private JLabel getLblMembre() {
		if (lblMembre == null) {
			lblMembre = new JLabel("Prix");
			lblMembre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblMembre;
	}

	private JPanel getPanel_2_1() {
		if (pnlPhoto == null) {
			pnlPhoto = new JPanel();
			pnlPhoto.setLayout(new GridLayout(2, 1, 0, 3));
			pnlPhoto.add(getLblPhoto());
			pnlPhoto.add(getButton());
		}
		return pnlPhoto;
	}

	private JLabel getLblPhoto() {
		if (lblPhoto == null) {
			lblPhoto = new JLabel("");
			lblPhoto.setFont(new Font("Tahoma", Font.PLAIN, 36));
		}
		return lblPhoto;
	}

	private JPanel getPanel_4() {
		if (pnlField == null) {
			pnlField = new JPanel();
			pnlField.setLayout(new GridLayout(7, 1, 3, 5));
			pnlField.add(getTxtNumero());
			pnlField.add(getTextField());
			pnlField.add(getTextField_1());
			pnlField.add(getTextField_2());
			pnlField.add(getTextField_3());
			pnlField.add(getTextField_4());
			pnlField.add(getComboBox());
		}
		return pnlField;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTableAlbum());
		}
		return scrollPane;
	}

	private JTextField getTxtNumero() {
		if (txtNumero == null) {
			txtNumero = new JTextField();
			txtNumero.setEnabled(false);
			;
			txtNumero.setColumns(10);
		}
		return txtNumero;
	}

	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setEnabled(false);
			textField.setColumns(10);
		}
		return textField;
	}

	private JTextField getTextField_1() {
		if (textField_1 == null) {
			textField_1 = new JTextField();
			textField_1.setEnabled(false);
			textField_1.setColumns(10);
		}
		return textField_1;
	}

	private JTable getTableAlbum() {
		if (tableAlbum == null) {
			tableAlbum = new JTable(modeleAlbum);
			tableAlbum.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableAlbum.getSelectionModel().addListSelectionListener(new GestionClick());
			tableAlbum.addMouseListener(new GestionClick());

		}
		return tableAlbum;
	}

	private JLabel getLblGenre() {
		if (lblGenre == null) {
			lblGenre = new JLabel("Genre");
			lblGenre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblGenre;
	}

	private JLabel getLblAnneeDistribution() {
		if (lblAnneeDistribution == null) {
			lblAnneeDistribution = new JLabel("Anne Dist");
			lblAnneeDistribution.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblAnneeDistribution;
	}

	private JLabel getLblMaisonDistribution() {
		if (lblMaisonDistribution == null) {
			lblMaisonDistribution = new JLabel("Maison Dist");
			lblMaisonDistribution.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblMaisonDistribution;
	}

	private JLabel getLblNumroArtiste() {
		if (lblNumroArtiste == null) {
			lblNumroArtiste = new JLabel("Num\u00E9ro Artiste");
			lblNumroArtiste.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNumroArtiste;
	}

	private JTextField getTextField_2() {
		if (textField_2 == null) {
			textField_2 = new JTextField();
			textField_2.setEnabled(false);
			textField_2.setColumns(10);
		}
		return textField_2;
	}

	private JTextField getTextField_3() {
		if (textField_3 == null) {
			textField_3 = new JTextField();
			textField_3.setEnabled(false);
			textField_3.setColumns(10);
		}
		return textField_3;
	}

	private JTextField getTextField_4() {
		if (textField_4 == null) {
			textField_4 = new JTextField();
			textField_4.setEnabled(false);
			textField_4.setColumns(10);
		}
		return textField_4;
	}

	private JComboBox getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox();
			comboBox.setEnabled(false);
			GestionArtistes gestionArtistes = new GestionArtistes();
			listArtiste = gestionArtistes.getListeArtistes();
			for (Artistes artistes : listArtiste) {
				comboBox.addItem(artistes.getNom());
			}
		}
		return comboBox;
	}

	private JButton getButton() {
		if (button == null) {
			button = new JButton("");
			button.addActionListener(new ButtonActionListener());
			button.setVisible(false);
		}
		return button;
	}

	private class GestionClick extends MouseAdapter implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				int numeroLigne;
				String nomArtiste = "";
				int index = 0;
				boolean verifier = false;
				numeroLigne = tableAlbum.getSelectedRow();
				txtNumero.setText(String.valueOf(tableAlbum.getValueAt(numeroLigne, 0)));
				textField.setText(String.valueOf(tableAlbum.getValueAt(numeroLigne, 1)));
				textField_1.setText(String.valueOf(tableAlbum.getValueAt(numeroLigne, 2)));
				textField_2.setText(String.valueOf(tableAlbum.getValueAt(numeroLigne, 3)));
				textField_3.setText(String.valueOf(tableAlbum.getValueAt(numeroLigne, 4)));
				textField_4.setText(String.valueOf(tableAlbum.getValueAt(numeroLigne, 5)));
				String numArti = String.valueOf(tableAlbum.getValueAt(numeroLigne, 7));
				for (Artistes artistes : listArtiste) {
					if (artistes.getNumero().equals(numArti)) {
						nomArtiste = artistes.getNom();
					}
				}
				int count = comboBox.getItemCount();
				while (index <= comboBox.getItemCount() || !verifier) {
					if (comboBox.getItemAt(index) == nomArtiste) {
						comboBox.setSelectedIndex(index);
						verifier = true;
					}
					index++;
				}
				Album album = liste.getListeAlbum().get(tableAlbum.getSelectedRow());
				lblPhoto.removeAll();
				lblPhoto.setIcon(new ImageIcon("C:/images/albums/"+album.getImage()));
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				textField.setEnabled(true);
				textField_1.setEnabled(true);
				textField_2.setEnabled(true);
				textField_3.setEnabled(true);
				textField_4.setEnabled(true);
				comboBox.setEnabled(true);
				button.setText("Modifier");
				button.setActionCommand("Modifier");
				button.setVisible(true);
				button.setEnabled(true);
				button.setCursor(new Cursor(Cursor.HAND_CURSOR));

			} else {
				textField.setEnabled(false);
				textField_1.setEnabled(false);
				textField_2.setEnabled(false);
				textField_3.setEnabled(false);
				textField_4.setEnabled(false);
				comboBox.setEnabled(false);
			}
		}

	}

	private class LblMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getSource() == lblAjouter) {
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				txtNumero.setText("");
				textField.setEnabled(true);
				textField_1.setEnabled(true);
				textField_2.setEnabled(true);
				textField_3.setEnabled(true);
				textField_4.setEnabled(true);
				comboBox.setEnabled(true);
				button.setText("Ajouter");
				button.setActionCommand("Ajouter");
				button.setVisible(true);
				
			} else if (e.getSource() == lblModifier) {
				if(!txtNumero.getText().isEmpty()){
					textField.setEnabled(true);
					textField_1.setEnabled(true);
					textField_2.setEnabled(true);
					textField_3.setEnabled(true);
					textField_4.setEnabled(true);
					comboBox.setEnabled(true);
					button.setText("Modifier");
					button.setActionCommand("Modifier");
					button.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Veuillez selectionner un album.","Modification",JOptionPane.INFORMATION_MESSAGE);
				}
				
				
			} else if (e.getSource() == lblSupprimer) {
				
				if(!txtNumero.getText().isEmpty()){
					int choix = JOptionPane.showConfirmDialog(null, "\u00CAtes-vous s\u00FBr de vouloir supprimer cet album ?","Suppression d'artiste",JOptionPane.YES_NO_OPTION);
					if(choix == JOptionPane.YES_OPTION){
						Album album = liste.getListeAlbum().get(tableAlbum.getSelectedRow());
						boolean supp = liste.supprimerAlbum(album);
						if(supp ==true){
							modeleAlbum.supprimerAlbumBD(album);
						}
					}
				} else {
					JOptionPane.showMessageDialog(null, "Veuillez selectionner un album.","Supression",JOptionPane.INFORMATION_MESSAGE);
				}
				
			} else if (e.getSource() == lblRechercher) {

				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				txtNumero.setText("");
				txtNumero.setEnabled(true);
				textField.setEnabled(false);
				textField_1.setEnabled(false);
				textField_2.setEnabled(false);
				textField_3.setEnabled(false);
				textField_4.setEnabled(false);
				comboBox.setEnabled(false);
				
				button.setText("Rechercher");
				button.setActionCommand("Rechercher");
				button.setVisible(true);

			} else if (e.getSource() == lblQuitter) {
				int rep =JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter cette page ?","Fermeture",JOptionPane.YES_NO_OPTION);
				if(rep == JOptionPane.YES_OPTION){
				FrmMenu frame = new FrmMenu();
				frame.setVisible(true);
				dispose();
				}
			}
		}
	}

	private class ButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			txtNumero.setEnabled(false);
			textField.setEnabled(false);
			textField_1.setEnabled(false);
			textField_2.setEnabled(false);
			textField_3.setEnabled(false);
			textField_4.setEnabled(false);
			comboBox.setEnabled(false);
			button.setVisible(false);
			boolean trouver = false;

			if (e.getActionCommand().equalsIgnoreCase("Ajouter")) {

				String titre = textField.getText();
				double prix = Double.parseDouble(textField_1.getText());
				String genre = textField_2.getText();
				String annee = textField_3.getText();
				String maison = textField_4.getText();
				button.setVisible(false);
				int numero = 0;
				for (Artistes artistes : listArtiste) {
					if (comboBox.getSelectedItem() == artistes.getNom()) {
						numero = Integer.parseInt(artistes.getNumero());
					}
				}
				JFileChooser jFileChooser = new JFileChooser();
				File repertoire = new File("C:\\images\\albums");
				jFileChooser.setCurrentDirectory(repertoire);
				String photo = "";
				if (jFileChooser.showOpenDialog(FrmAfficherAlbum.this) == JFileChooser.APPROVE_OPTION) {
					photo = jFileChooser.getSelectedFile().getName();
				}
				Album album = new Album(0, titre, prix, genre, annee, maison, photo, numero);
				Iterator<Album> iterateur = liste.getListeAlbum().iterator();
				while (iterateur.hasNext() && !trouver) {
					Album albumPresent = iterateur.next();
					if (album.getTitre().equalsIgnoreCase(albumPresent.getTitre())) {
						trouver = true;
						JOptionPane.showMessageDialog(null, "L'album est d\u00E9j� pr\u00E9sent", "Ajout",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
				if(trouver == false){
					int choix = JOptionPane.showConfirmDialog(null, "Ajouter " + album.getTitre() + " ?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
					if(choix == JOptionPane.YES_OPTION){
						boolean ajouter = liste.ajouterAlbum(album);
						if (ajouter) {
							int id = liste.getId(album);
							album.setId(id);
							modeleAlbum.ajouterAlbum(album);
						}
					}
				}
			} else if (e.getActionCommand().equalsIgnoreCase("Modifier")) {

				Album album = liste.getListeAlbum().get(tableAlbum.getSelectedRow());
				int index = tableAlbum.getSelectedRow();
				album.setTitre(textField.getText());
				album.setPrix(Double.parseDouble(textField_1.getText()));
				album.setGenre(textField_2.getText());
				album.setAnneeDeSortie(textField_3.getText());
				album.setMaisonDistribution(textField_4.getText());
				boolean modifier = liste.modifierAlbum(album);
				if (modifier) {
					modeleAlbum.modifierAlbum(index, album);
				}
			} else if (e.getActionCommand().equalsIgnoreCase("Rechercher")){
				
				String id = txtNumero.getText();
				ArrayList<Album> listeRecherche = liste.rechercherAlbum(id);
				modeleAlbum.setLesDonnees(listeRecherche);
				tableAlbum.setModel(modeleAlbum);

				if(listeRecherche.isEmpty()){
					JOptionPane.showMessageDialog(null, "Il n'y a aucun album qui correspond � votre recherche", "Recherche", JOptionPane.INFORMATION_MESSAGE);
				}
				
				button.setText("Revenir");
				button.setVisible(true);
				button.setActionCommand("Revenir");
			} else if(e.getActionCommand() == "Revenir"){
				modeleAlbum = new ModeleAlbum( liste.getListeAlbum());
				tableAlbum.setModel(modeleAlbum);
				
			}
		}
	}
	private class ThisWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			
			int rep =JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter cette page ?","Fermeture",JOptionPane.YES_NO_OPTION);
			if(rep == JOptionPane.YES_OPTION){
				FrmAfficherAlbum.this.dispose();
				new FrmMenu().setVisible(true);;
			}
		}
	}
}

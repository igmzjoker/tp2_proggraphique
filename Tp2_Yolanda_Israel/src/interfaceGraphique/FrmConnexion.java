package interfaceGraphique;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gestionDonnees.ControleConnexion;
import gestionDonnees.GestionUtilisateur;
import gestionDonnees.Utilisateur;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FrmConnexion extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField textArea;
	private JLabel lblConnexion;
	private JLabel lblNomDutilisateur;
	private JLabel lblMotDePasse;
	private JLabel label;
	private GestionUtilisateur gestionUtilisateur;
	private ArrayList<Utilisateur> liste;

	/**
	 * Create the frame.
	 */
	public FrmConnexion() {
		setResizable(false);
		addWindowListener(new ThisWindowListener());
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmConnexion.class.getResource("/images/album2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JButton btnValider = new JButton("Valider");
		getRootPane().setDefaultButton(btnValider);
		btnValider.addActionListener(new BtnValiderActionListener());
		btnValider.setIcon(new ImageIcon(FrmConnexion.class.getResource("/images/check.png")));
		btnValider.setBounds(66, 211, 117, 29);
		btnValider.setActionCommand("valider");
		btnValider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		contentPane.add(btnValider);

		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.addActionListener(new BtnValiderActionListener());
		btnQuitter.setIcon(new ImageIcon(FrmConnexion.class.getResource("/images/icon_quit.gif")));
		btnQuitter.setBounds(254, 211, 117, 29);
		btnQuitter.setActionCommand("quitter");
		btnQuitter.setCursor(new Cursor(Cursor.HAND_CURSOR));
		contentPane.add(btnQuitter);
		contentPane.add(getPasswordField_1());
		contentPane.add(getTextArea());
		contentPane.add(getLblConnexion());
		contentPane.add(getLabel_1());
		contentPane.add(getLabel_2());
		contentPane.add(getLabel());
	}

	private JPasswordField getPasswordField_1() {
		if (passwordField == null) {
			passwordField = new JPasswordField();
			passwordField.setBounds(217, 142, 168, 25);
		}
		return passwordField;
	}

	private JTextField getTextArea() {
		if (textArea == null) {
			textArea = new JTextField();
			textArea.setBounds(217, 87, 168, 22);
		}
		return textArea;
	}

	private JLabel getLblConnexion() {
		if (lblConnexion == null) {
			lblConnexion = new JLabel("Connexion au Programme");
			lblConnexion.setIcon(new ImageIcon(FrmConnexion.class.getResource("/images/world-icon.png")));
			lblConnexion.setFont(new Font("Lucida Grande", Font.PLAIN, 22));
			lblConnexion.setBounds(23, 26, 381, 49);
		}
		return lblConnexion;
	}

	private JLabel getLabel_1() {
		if (lblNomDutilisateur == null) {
			lblNomDutilisateur = new JLabel("Nom d'utilisateur");
			lblNomDutilisateur
					.setIcon(new ImageIcon(FrmConnexion.class.getResource("/images/Administrator-icon-2.png")));
			lblNomDutilisateur.setBounds(56, 93, 134, 16);
		}
		return lblNomDutilisateur;
	}

	private JLabel getLabel_2() {
		if (lblMotDePasse == null) {
			lblMotDePasse = new JLabel("Mot de passe");
			lblMotDePasse.setIcon(new ImageIcon(FrmConnexion.class.getResource("/images/secrecy-icon.png")));
			lblMotDePasse.setBounds(56, 147, 119, 16);
		}
		return lblMotDePasse;
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("");
			label.setIcon(new ImageIcon(FrmConnexion.class.getResource("/images/ban.png")));
			label.setBounds(6, 16, 404, 183);
		}
		return label;
	}

	private class BtnValiderActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand()=="valider") {
				ControleConnexion.connecter();
				gestionUtilisateur = new GestionUtilisateur();
				liste = gestionUtilisateur.getListeUtilisateur();
				String utili = textArea.getText();
				String motPasse = String.valueOf(passwordField.getPassword());
				int compteur = 0;
				for (Utilisateur utilisateur : liste) {
					if(utilisateur.getUtilisateur().equalsIgnoreCase(utili)){
						if(utilisateur.getPassword().equalsIgnoreCase(motPasse)){
							FrmMenu menu = new FrmMenu();
							menu.setVisible(true);
							textArea.setText("");
							passwordField.setText("");
							break;
						}else{
							JOptionPane.showMessageDialog(null, "Mot de passe erron\u00E9","Erreur",JOptionPane.ERROR_MESSAGE);
						}
					}else{
						compteur++;
					}
				}
				if(compteur == liste.size()){
					JOptionPane.showMessageDialog(null, "Ce nom d'utilisateur : "+ utili +"  n'existe pas","Erreur",JOptionPane.ERROR_MESSAGE);
				}
			} else if(e.getActionCommand() == "quitter"){
				int choix = JOptionPane.showConfirmDialog(null, "Voulez-vous quitter l'application ?", "Fermeture", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(choix == JOptionPane.YES_OPTION){
				dispose();
				ControleConnexion.fermerSession();
				}
			}
		}
	}
	private class ThisWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			int choix = JOptionPane.showConfirmDialog(null, "Voulez-vous quitter l'application ?", "Fermeture", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(choix == JOptionPane.YES_OPTION){
			dispose();
			ControleConnexion.fermerSession();
			}
		}
	}

}

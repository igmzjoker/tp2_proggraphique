package interfaceGraphique;
import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JButton;
import javax.swing.JTable;

import gestionDonnees.Album;
import gestionDonnees.Artistes;
import gestionDonnees.GestionAlbum;
import gestionDonnees.GestionArtistes;
import gestionDonnees.ModeleArtiste;

import javax.swing.DefaultListModel;

import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;

public class FrmAfficherArtistes extends JFrame  {

	private JPanel contentPane;
	private JPanel pnlMenu;
	private JLabel lblAjouter;
	private JLabel lblModifier;
	private JLabel lblSupprimer;
	private JLabel lblRechercher;
	private JLabel lblQuitter;
	private JPanel pnlCenter;
	private JPanel pnlArtiste;
	private JPanel pnlinfo;
	private JLabel lblNumro;
	private JLabel lblNom;
	private JLabel lblMembre;
	private JPanel pnlPhoto;
	private JLabel lblPhoto;
	public DefaultListModel<String> modeleAlbum = new DefaultListModel<String>();
	private JList<String> listAlbum;
	private JPanel pnlField;
	private JScrollPane scrollPane;
	private JTextField txtNumero;
	private JTextField textFieldNom;
	private static GestionArtistes liste;
	private static ModeleArtiste  modeleArtiste;
	private JTable tableArtiste;
	private JCheckBox checkBoxMembre;
	private static GestionAlbum album;
	private JButton btnModifier;
	private ArrayList<Artistes> nouvListeRecherche = new ArrayList<Artistes>();

	/**
	 * Create the frame.
	 */
	public FrmAfficherArtistes() {
		liste = new GestionArtistes();
		album = new GestionAlbum();
		modeleArtiste = new ModeleArtiste(liste.getListeArtistes());
		addWindowListener(new ThisWindowListener());
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmAfficherArtistes.class.getResource("/images/album2.png")));
		setTitle("Gestion des artistes");
		setBounds(100, 100, 544, 364);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanel_2(), BorderLayout.WEST);
		contentPane.add(getPanel_1_1(), BorderLayout.CENTER);
	}
	private JPanel getPanel_2() {
		if (pnlMenu == null) {
			pnlMenu = new JPanel();
			pnlMenu.setLayout(new GridLayout(5, 1, 0, 0));
			pnlMenu.add(getLblAjouter());
			pnlMenu.add(getLblModifier());
			pnlMenu.add(getLblSupprimer());
			pnlMenu.add(getLblRechercher());
			pnlMenu.add(getLblQuitter());
		}
		return pnlMenu;
	}
	private JLabel getLblAjouter() {
		if (lblAjouter == null) {
			lblAjouter = new JLabel("Ajouter");
			lblAjouter.addMouseListener(new LblAjouterMouseListener());
			lblAjouter.setIcon(new ImageIcon(FrmAfficherArtistes.class.getResource("/images/add.png")));
			lblAjouter.setHorizontalAlignment(SwingConstants.CENTER);
			lblAjouter.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblAjouter.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return lblAjouter;
	}
	private JLabel getLblModifier() {
		if (lblModifier == null) {
			lblModifier = new JLabel("Modifier");
			lblModifier.addMouseListener(new LblModifierMouseListener());
			lblModifier.setIcon(new ImageIcon(FrmAfficherArtistes.class.getResource("/images/edit.png")));
			lblModifier.setHorizontalAlignment(SwingConstants.CENTER);
			lblModifier.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblModifier.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return lblModifier;
	}
	private JLabel getLblSupprimer() {
		if (lblSupprimer == null) {
			lblSupprimer = new JLabel("Supprimer");
			lblSupprimer.addMouseListener(new LblSupprimerMouseListener());
			lblSupprimer.setIcon(new ImageIcon(FrmAfficherArtistes.class.getResource("/images/delete.png")));
			lblSupprimer.setHorizontalAlignment(SwingConstants.CENTER);
			lblSupprimer.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblSupprimer.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return lblSupprimer;
	}
	private JLabel getLblRechercher() {
		if (lblRechercher == null) {
			lblRechercher = new JLabel("Rechercher");
			lblRechercher.addMouseListener(new LblRechercherMouseListener());
			lblRechercher.setIcon(new ImageIcon(FrmAfficherArtistes.class.getResource("/images/search.png")));
			lblRechercher.setHorizontalAlignment(SwingConstants.CENTER);
			lblRechercher.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRechercher.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return lblRechercher;
	}
	private JLabel getLblQuitter() {
		if (lblQuitter == null) {
			lblQuitter = new JLabel("Quitter");
			lblQuitter.addMouseListener(new LblQuitterMouseListener());
			lblQuitter.setIcon(new ImageIcon(FrmAfficherArtistes.class.getResource("/images/icon_quit.gif")));
			lblQuitter.setHorizontalAlignment(SwingConstants.CENTER);
			lblQuitter.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblQuitter.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return lblQuitter;
	}
	private JPanel getPanel_1_1() {
		if (pnlCenter == null) {
			pnlCenter = new JPanel();
			pnlCenter.setLayout(new BorderLayout(0, 0));
			pnlCenter.add(getPanel_3(), BorderLayout.NORTH);
			pnlCenter.add(getScrollPane(), BorderLayout.CENTER);
		}
		return pnlCenter;
	}
	private JPanel getPanel_3() {
		if (pnlArtiste == null) {
			pnlArtiste = new JPanel();
			pnlArtiste.setLayout(new BorderLayout(10, 0));
			pnlArtiste.add(getPanel_1_2(), BorderLayout.WEST);
			pnlArtiste.add(getPanel_2_1(), BorderLayout.EAST);
			pnlArtiste.add(getPanel_4(), BorderLayout.CENTER);
			pnlArtiste.add(getBtnModifier(), BorderLayout.SOUTH);
	
		}
		return pnlArtiste;
	}
	private JPanel getPanel_1_2() {
		if (pnlinfo == null) {
			pnlinfo = new JPanel();
			pnlinfo.setLayout(new GridLayout(3, 1, 10, 0));
			pnlinfo.add(getLblNumro());
			pnlinfo.add(getLblNom());
			pnlinfo.add(getLblMembre());
		}
		return pnlinfo;
	}
	private JLabel getLblNumro() {
		if (lblNumro == null) {
			lblNumro = new JLabel("Num\u00E9ro");
			lblNumro.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNumro;
	}
	private JLabel getLblNom() {
		if (lblNom == null) {
			lblNom = new JLabel("Nom");
			lblNom.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNom;
	}
	private JLabel getLblMembre() {
		if (lblMembre == null) {
			lblMembre = new JLabel("Membre");
			lblMembre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblMembre;
	}
	private JPanel getPanel_2_1() {
		if (pnlPhoto == null) {
			pnlPhoto = new JPanel();
			pnlPhoto.setLayout(new GridLayout(2, 1, 0, 3));
			pnlPhoto.add(getLblPhoto());
			pnlPhoto.add(getListAlbum());
		}
		return pnlPhoto;
	}
	private JLabel getLblPhoto() {
		if (lblPhoto == null) {
			lblPhoto = new JLabel("");
			lblPhoto.setFont(new Font("Tahoma", Font.PLAIN, 36));
		}
		return lblPhoto;
	}
	private JList<String> getListAlbum() {
		if (listAlbum == null) {
			listAlbum = new JList<String>(modeleAlbum);
		}
		return listAlbum;
	}
	private JPanel getPanel_4() {
		if (pnlField == null) {
			pnlField = new JPanel();
			pnlField.setLayout(new GridLayout(3, 1, 3, 5));
			pnlField.add(getTxtNumero());
			pnlField.add(getTextFieldNom());
			pnlField.add(getCheckBoxMembre());
		}
		return pnlField;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTableArtiste());
		}
		return scrollPane;
	}
	private JTextField getTxtNumero() {
		if (txtNumero == null) {
			txtNumero = new JTextField();
			txtNumero.setEnabled(false);
			txtNumero.setColumns(10);
		}
		return txtNumero;
	}
	private JTextField getTextFieldNom() {
		if (textFieldNom == null) {
			textFieldNom = new JTextField();
			textFieldNom.setEnabled(false);
			textFieldNom.setColumns(10);
		}
		return textFieldNom;
	}
	private JTable getTableArtiste() {
		if (tableArtiste == null) {
			tableArtiste = new JTable(modeleArtiste);
			tableArtiste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableArtiste.getColumnModel().getColumn(0).setCellRenderer(new RendeurNumero());
			tableArtiste.getColumnModel().getColumn(2).setCellRenderer(new RendeurMembre());
			tableArtiste.getSelectionModel().addListSelectionListener(new GestionClick());
			tableArtiste.addMouseListener(new GestionClick());
		}
		return tableArtiste;
	}
	
	private class GestionClick extends MouseAdapter implements ListSelectionListener{
		
		ArrayList<Album>listeAlbumArtiste = new ArrayList<Album>();
		
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if(!e.getValueIsAdjusting()){
				Artistes artiste;
				int numLigne;
				numLigne = tableArtiste.getSelectedRow();
				if(numLigne != -1){
				txtNumero.setText(String .valueOf (tableArtiste.getValueAt(numLigne, 0)));
				textFieldNom.setText(String .valueOf (tableArtiste.getValueAt(numLigne, 1)));
				String membre = String .valueOf (tableArtiste.getValueAt(numLigne, 2));
				
				if(membre.equals("true")){
					checkBoxMembre.setSelected(true);
				}else {
					checkBoxMembre.setSelected(false);
				}
				if(!nouvListeRecherche.isEmpty()){
					artiste = nouvListeRecherche.get(tableArtiste.getSelectedRow());
				}else{
					artiste = liste.getListeArtistes().get(tableArtiste.getSelectedRow());
				}
				
				lblPhoto.setIcon(new ImageIcon("C:/images/artistes/"+artiste.getNomPhoto()));
				listeAlbumArtiste =  album.obtenirListeAlbumArtiste(Integer.parseInt(String .valueOf (tableArtiste.getValueAt(numLigne, 0))));
				
				if(listeAlbumArtiste.isEmpty()){
					modeleAlbum.removeAllElements();
				}else{
					for(int i = 0; i < listeAlbumArtiste.size(); i++){
						modeleAlbum.removeAllElements();
						modeleAlbum.addElement(listeAlbumArtiste.get(i).toString());
					}
				}
			}else {
				modeleAlbum.removeAllElements();
			}
			}
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			 if (e.getClickCount() == 2) {
				 textFieldNom.setEnabled(true);
				 checkBoxMembre.setEnabled(true);
				 btnModifier.setText("Modifier");
				 btnModifier.setEnabled(true);
				 btnModifier.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
	
				
			 } else {
				 textFieldNom.setEnabled(false);
				 checkBoxMembre.setEnabled(false);
				 btnModifier.setEnabled(false);
			 }
		}
	}

	private JCheckBox getCheckBoxMembre() {
		if (checkBoxMembre == null) {
			checkBoxMembre = new JCheckBox("");
			checkBoxMembre.setEnabled(false);
		}
		return checkBoxMembre;
	}
	private JButton getBtnModifier() {
		if (btnModifier == null) {
			btnModifier = new JButton();
			btnModifier.addActionListener(new BtnModifierActionListener());
			btnModifier.setEnabled(false);
		}
		return btnModifier;
	}
	private class BtnModifierActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			txtNumero.setEnabled(false);
			textFieldNom.setEnabled(false);
			checkBoxMembre.setEnabled(false);
			btnModifier.setEnabled(false);
			
			if (e.getActionCommand().equalsIgnoreCase("modifier")){
			
			Artistes artiste = liste.getListeArtistes().get(tableArtiste.getSelectedRow());
			int index = tableArtiste.getSelectedRow();
			artiste.setNom(textFieldNom.getText());
			artiste.setMembre(checkBoxMembre.isSelected());
			boolean modif = liste.modifierArtisteBD(artiste);
			 if(modif == true){
				  modeleArtiste.modifierArtiste(index,artiste);

			 }
			 
			} else if (e.getActionCommand().equalsIgnoreCase("ajouter")){
				JFileChooser jFileChooser = new JFileChooser();
				File repertoire = new File("C:\\images\\artistes");
				jFileChooser.setCurrentDirectory(repertoire);
				String photo = "";
				if(jFileChooser.showOpenDialog(FrmAfficherArtistes.this)== JFileChooser.APPROVE_OPTION){
					photo = jFileChooser.getSelectedFile().getName();
				}
				
				String nom = textFieldNom.getText();
				boolean membre = checkBoxMembre.isSelected();
				boolean trouve = false;
				Artistes artiste = new Artistes(nom,membre,photo);
				Iterator<Artistes> iterateur = liste.getListeArtistes().iterator();
				while(iterateur.hasNext() && !trouve){
					Artistes artistePresent = iterateur.next();
					if(artiste.getNom().equalsIgnoreCase(artistePresent.getNom())){
						trouve = true;
						JOptionPane.showMessageDialog(null, "L'artiste est d\u00E9j\u00E0 pr\u00E9sent","Ajout",JOptionPane.INFORMATION_MESSAGE);
					} 
				}
				if(trouve == false){
					int choix = JOptionPane.showConfirmDialog(null, "Ajouter " + artiste.getNom() + " ?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
					if(choix == JOptionPane.YES_OPTION){
						boolean ajout = liste.ajouterArtistesBD(artiste);
						if(ajout == true){
							String id = liste.getId(artiste);
							artiste.setNumero(id);
							modeleArtiste.ajouterArtiste(artiste);
						}
					}
				}

			} else if(e.getActionCommand().equalsIgnoreCase("rechercher")){
				String id = txtNumero.getText();
				String name = textFieldNom.getText();
				ArrayList<Artistes> listeRecherche = liste.rechercherArtiste(id, name);
				nouvListeRecherche = listeRecherche;
				modeleArtiste.setLesDonnees(listeRecherche);
				tableArtiste.setModel(modeleArtiste);
				if(listeRecherche.isEmpty()){
					JOptionPane.showMessageDialog(null, "Il n'y a aucun artiste qui correspond � votre recherche", "Recherche", JOptionPane.INFORMATION_MESSAGE);
				}
				btnModifier.setEnabled(true);
				btnModifier.setText("Revenir");
				btnModifier.setActionCommand("revenir");
				
			}else if(e.getActionCommand().equalsIgnoreCase("revenir")){
				nouvListeRecherche.clear();
				modeleArtiste.setLesDonnees(liste.getListeArtistes());
				tableArtiste.setModel(modeleArtiste);
				btnModifier.setEnabled(false);
			}
			
			txtNumero.setText("");
			textFieldNom.setText("");
			checkBoxMembre.setSelected(false);
		}
	}
	private class LblModifierMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			
			if(!txtNumero.getText().isEmpty()){
			textFieldNom.setEnabled(true);
			checkBoxMembre.setEnabled(true);
			btnModifier.setEnabled(true);
			btnModifier.setText("Modifier");
			btnModifier.setActionCommand("modifier");
			btnModifier.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
			} else {
				JOptionPane.showMessageDialog(null, "Veuillez selectionner un artiste.", "Modification d'artiste", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	private class LblAjouterMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			
			btnModifier.setText("Ajouter");
			btnModifier.setActionCommand("ajouter");
			txtNumero.setText("");
			textFieldNom.setText("");
			checkBoxMembre.setSelected(false);

			txtNumero.setEnabled(false);
			textFieldNom.setEnabled(true);
			checkBoxMembre.setEnabled(true);
			btnModifier.setEnabled(true);
			lblPhoto.setIcon(null);
			modeleAlbum.removeAllElements();
			

		}
	}
	private class LblSupprimerMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if(!txtNumero.getText().isEmpty()){
				if(modeleAlbum.isEmpty()){
					int choix = JOptionPane.showConfirmDialog(null, "\u00CAtes-vous s\u00FBr de vouloir supprimer cet artiste ?","Suppression d'artiste",JOptionPane.YES_NO_OPTION);
					if(choix == JOptionPane.YES_OPTION){
						Artistes artiste = liste.getListeArtistes().get(tableArtiste.getSelectedRow());
						boolean supp = liste.supprimerArtisteBD(artiste);
						if(supp==true){
							modeleArtiste.supprimerArtiste(artiste);
							
						}
					}
				} else{
					JOptionPane.showMessageDialog(null, "L'artiste ne peut pas \u00EAtre supprim\u00E9, car il poss\u00E8de des albums.","Erreur", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "Veuillez selectionner un artiste.","Supression",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	private class LblQuitterMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			int rep =JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter cette page ?","Fermeture",JOptionPane.YES_NO_OPTION);
			if(rep == JOptionPane.YES_OPTION){
				FrmAfficherArtistes.this.dispose();
				new FrmMenu().setVisible(true);
			}
		}
	}
	private class ThisWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			int rep =JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter cette page ?","Fermeture",JOptionPane.YES_NO_OPTION);
			if(rep == JOptionPane.YES_OPTION){
				FrmAfficherArtistes.this.dispose();
				new FrmMenu().setVisible(true);
			}
		}
	}
	private class LblRechercherMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			textFieldNom.setEnabled(true);
			txtNumero.setEnabled(true);
			textFieldNom.setText("");
			txtNumero.setText("");
			btnModifier.setEnabled(true);
			btnModifier.setText("Rechercher");
			btnModifier.setActionCommand("rechercher");
			btnModifier.setCursor(new Cursor(Cursor.HAND_CURSOR));
			lblPhoto.setIcon(null);
			modeleAlbum.removeAllElements();
		}
	}

}

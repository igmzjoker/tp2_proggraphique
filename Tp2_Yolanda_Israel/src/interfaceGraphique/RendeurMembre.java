package interfaceGraphique;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;


public class RendeurMembre extends DefaultTableCellRenderer  {
	private Icon iconeMembre;
	private Icon icone;
	
	public RendeurMembre() {
		iconeMembre =new ImageIcon(getToolkit().getImage(RendeurMembre.class.getResource("/images/membre.png")));
		icone = new ImageIcon(getToolkit().getImage(RendeurMembre.class.getResource("/images/check.png")));
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,int row, int column){
	
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		boolean membre = (boolean) value;
		if(membre){
			setIcon(iconeMembre);
		}
		else{
			setIcon(icone);
		}


		setHorizontalAlignment(CENTER);
		return this;
	}

}

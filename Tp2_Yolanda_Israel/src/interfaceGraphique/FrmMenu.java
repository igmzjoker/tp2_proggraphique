package interfaceGraphique;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FrmMenu extends JFrame {

	private JPanel contentPane;
	private JLabel btnArtiste;
	private JLabel btnAlbum;
	private JLabel btnQuitter;
	private JLabel btnPropos;
	private JLabel lblMenuDeLapplication;

	/**
	 * Create the frame.
	 */
	public FrmMenu() {
		setResizable(false);
		addWindowListener(new ThisWindowListener());
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmMenu.class.getResource("/images/album2.png")));
		setTitle("Menu de l'application");
		setBounds(100, 100, 450, 300);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getBtnArtiste());
		contentPane.add(getBtnAlbum());
		contentPane.add(getBtnQuitter());
		contentPane.add(getBtnPropos());
		contentPane.add(getLblMenuDeLapplication());
	}
	private JLabel getBtnArtiste() {
		if (btnArtiste == null) {
			btnArtiste = new JLabel(new ImageIcon(FrmMenu.class.getResource("/images/Actions-view-media-artist-icon.png")));
			btnArtiste.addMouseListener(new BtnArtisteMouseListener());
			btnArtiste.setBounds(14, 57, 171, 57);
			btnArtiste.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return btnArtiste;
	}
	private JLabel getBtnAlbum() {
		if (btnAlbum == null) {
			btnAlbum = new JLabel(new ImageIcon(FrmMenu.class.getResource("/images/CD-icon.png")));
			btnAlbum.addMouseListener(new BtnAlbumMouseListener());
			btnAlbum.setBounds(14, 125, 168, 57);
			btnAlbum.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return btnAlbum;
	}
	private JLabel getBtnQuitter() {
		if (btnQuitter == null) {
			btnQuitter = new JLabel(new ImageIcon(FrmMenu.class.getResource("/images/Button-Log-Off-icon.png")));
			btnQuitter.addMouseListener(new BtnQuitterMouseListener());
			btnQuitter.setBounds(28, 193, 154, 57);
			btnQuitter.setCursor(new Cursor(Cursor.HAND_CURSOR)); 
		}
		return btnQuitter;
	}
	private JLabel getBtnPropos() {
		if (btnPropos == null) {
			btnPropos = new JLabel(new ImageIcon(FrmMenu.class.getResource("/images/Actions-help-about-icon.png")));
			btnPropos.addMouseListener(new BtnProposMouseListener());
			btnPropos.setBounds(252, 207, 144, 43);
			btnPropos.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		return btnPropos;
	}
	private JLabel getLblMenuDeLapplication() {
		if (lblMenuDeLapplication == null) {
			lblMenuDeLapplication = new JLabel("Menu de L'application");
			lblMenuDeLapplication.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/Very-Basic-Menu-icon-2.png")));
			lblMenuDeLapplication.setFont(new Font("Lucida Grande", Font.PLAIN, 26));
			lblMenuDeLapplication.setBounds(41, 19, 332, 29);
		}
		return lblMenuDeLapplication;
	}
	private class BtnArtisteMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked (MouseEvent e){
			new FrmAfficherArtistes().setVisible(true);
			dispose();
		}
		@Override
		public void mouseEntered(MouseEvent e){
			btnArtiste.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/artiste.png")));
			btnArtiste.setText("Artistes");
		}
		
		@Override
		public void mouseExited(MouseEvent e){
			btnArtiste.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/Actions-view-media-artist-icon.png")));
			btnArtiste.setText("");
		}
		
	}
	private class BtnAlbumMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked (MouseEvent e) {
			new FrmAfficherAlbum().setVisible(true);
			dispose();
		}
		
		@Override
		public void mouseEntered(MouseEvent e){
			btnAlbum.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/CD.png")));
			btnAlbum.setText("Albums");
		}
		
		@Override
		public void mouseExited(MouseEvent e){
			btnAlbum.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/CD-icon.png")));
			btnAlbum.setText("");
		}
		
		
	}
	private class BtnQuitterMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			int choix = JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment vous d\u00E9connecter ?", "Déconnexion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(choix == JOptionPane.YES_OPTION){
				FrmMenu.this.dispose();
				
			}
		}
		
		@Override
		public void mouseEntered(MouseEvent e){
			btnQuitter.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/deconnexion.png")));
			btnQuitter.setText("D\u00E9connexion");
		}
		
		@Override
		public void mouseExited(MouseEvent e){
			btnQuitter.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/Button-Log-Off-icon.png")));
			btnQuitter.setText("");
		}
		
	}
	
	private class BtnProposMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e){
			new Apropos(FrmMenu.this).setVisible(true);;
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			btnPropos.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/help.png")));
			btnPropos.setText("\u00C0 propos");
		}
		@Override
		public void mouseExited(MouseEvent e){
			btnPropos.setIcon(new ImageIcon(FrmMenu.class.getResource("/images/Actions-help-about-icon.png")));
			btnPropos.setText("");
		}
	}
	private class ThisWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			int choix = JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment vous d\u00E9connecter ?", "Fermeture", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(choix == JOptionPane.YES_OPTION){
			dispose();
			}
		}
	}
}

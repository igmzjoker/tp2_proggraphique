package interfaceGraphique;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class RendeurNumero extends DefaultTableCellRenderer  {

	public RendeurNumero() {

	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,int row, int column){
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		String numero = (String)value;
		setHorizontalAlignment(CENTER);
		setFont(new Font("",Font.BOLD,12));
		return this;
		
	}

}
